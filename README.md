# kubernetes

## 介绍
CentOS7版本部署K8S集群

## 软件架构
kubeadm是官方社区推出的一个用于快速部署kubernetes集群的工具


## 安装教程

### 一、安装要求
- 至少两台服务器，操作系统为CentOS7.X-86_x64
- 硬件配置：2GB或更多RAM，2个CPU或更多CPU，硬盘30GB或更多
- 集群中所有机器之间网络互通
- 可以访问外网，需要拉取镜像(或者提前准备好镜像)
### 二、准备环境
```bash
三台主机
IP:  192.168.101.2     主机名：master    系统： centos 7.6      配置： 2C 2G
IP:  192.168.101.3     主机名：node1     系统： centos 7.6      配置： 2C 2G
IP:  192.168.101.4     主机名：node2     系统： centos 7.6      配置： 2C 2G
```
### 三、安装步骤
- 基于K8S集群进行操作
#### 1、所有节点安装nfs服务
```bash
yum -y install nfs-utils
systemctl start nfs
systemctl enable nfs
```
#### 2、部署nfs pv
- 解压上传的nfs-client.tar.gz
```bash
tar zxvf nfs-client.tar.gz
cd nfs-client
# 查看确认里面有三个文件
# class.yaml  deployment.yaml  rbac.yaml

kubectl apply -f rbac.yaml
kubectl apply -f class.yaml
kubectl get sc
```
#### 3、指定一台nfs服务端
- 这里以node1节点为例
```bash
cat > /etc/exports << EOF
/mnt *(rw,no_root_squash)
EOF
systemctl restart nfs
mkdir /mnt/kubernetes
```
#### 4、部署deployment.yaml
- 修改deployment.yaml
![1](./image/1.png)
- 部署
```bash
kubectl apply -f deployment.yaml
```
#### 5、部署Prometheus+Grafana
##### 5.1解压prometheus.tar.gz
```bash
tar zxvf prometheus.tar.gz
```
##### 5.2、确认文件
![2](./image/2.png)
##### 5.3、修改prometheus-configmap.yaml
![3](./image/3.png)
##### 5.4、部署prometheus
```bash
kubectl apply -f prometheus-configmap.yaml
kubectl apply -f prometheus-rbac.yaml
kubectl apply -f prometheus-rules.yaml
kubectl apply -f prometheus-service.yaml
kubectl apply -f prometheus-statefulset.yaml
```
- 访问prometheus： node_ip:30090
##### 5.5、部署Grafana
```bash
kubectl apply -f grafana.yaml
```
- 访问Grafana：node_ip:30007
- 初始账号密码都是admin
##### 5.6、添加监控node节点
```bash
kubectl apply -f node-exporter-ds.yml
```
##### 5.7、添加监控master节点
```bash
kubectl apply -f kube-state-metrics-rbac.yaml
kubectl apply -f kube-state-metrics-service.yaml
kubectl apply -f kube-state-metrics-deployment.yaml
```
##### 5.8、grafana添加数据源
![4](./image/4.png)
![5](./image/5.png)
![6](./image/6.png)
- 在master节点查看prometheus的svc
```bash
kubectl get svc -n kube-system
```
![7](./image/7.png)
- 通过name+port就可以连接
![8](./image/8.png)

#### 使用说明

1.  本文章测试使用的集群版本是1.16.0
2.  使用提供的文件
```bash
nfs-client.tar.gz
prometheus.tar.gz
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

